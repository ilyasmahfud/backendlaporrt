package com.project.laporRTeApps.seeder;

import com.project.laporRTeApps.module.authentication.entity.User;
import com.project.laporRTeApps.module.authentication.entity.UserDetails;
import com.project.laporRTeApps.module.authentication.repository.UserRepository;
import com.project.laporRTeApps.module.authentication.service.AuthenticationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(value = 3)
@Slf4j
public class UserSeeder implements CommandLineRunner {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AuthenticationService authenticationService;

    @Override
    public void run(String... args) {
        try {
            seed();
        } catch (Exception exception) {
            log.error(exception.getMessage());
        }
    }

    private void seed() {
        if (userRepository.count() == 0) {
            User userTony = new User();
            userTony.setEmail("tonystark@gmail.com");
            userTony.setPassword("password");

            UserDetails userDetailsTony = new UserDetails();
            userDetailsTony.setFullName("Tony Stark");
            userDetailsTony.setMobileNumber("082333444555");
            userDetailsTony.setNik("3309132255000005");
            userDetailsTony.setProfilePicture("https://cdn.kibrispdr.org/data/icon-profile-png-0.jpg");
            userDetailsTony.setPlaceOfBirth("Jakarta");
            userDetailsTony.setDateOfBirth("12/12/1992");
            userDetailsTony.setGender("Laki-laki");
            userDetailsTony.setAddress("Jl. Metro Raya, Pondok Indah, Jakarta Selatan, DKI Jakarta");
            userDetailsTony.setRt("15");
            userDetailsTony.setWork("Wirausaha");
            userDetailsTony.setReligion("Islam");
            userDetailsTony.setMaritalStatus("Kawin");
            userDetailsTony.setNationality("WNI");

            authenticationService.register(userTony, userDetailsTony);

            User userBruce = new User();
            userBruce.setEmail("brucewayne@gmail.com");
            userBruce.setPassword("password");

            UserDetails userDetailsBruce = new UserDetails();
            userDetailsBruce.setFullName("Bruce Wayne");
            userDetailsBruce.setMobileNumber("081333444555");
            userDetailsBruce.setNik("3309132255000006");
            userDetailsBruce.setProfilePicture("https://cdn.kibrispdr.org/data/icon-profile-png-0.jpg");
            userDetailsBruce.setPlaceOfBirth("Surabaya");
            userDetailsBruce.setDateOfBirth("9/9/1999");
            userDetailsBruce.setGender("Laki-laki");
            userDetailsBruce.setAddress("Jl. Syamsu Rizal, Menteng, Jakarta Pusat, DKI Jakarta");
            userDetailsBruce.setRt("15");
            userDetailsBruce.setWork("Karyawan");
            userDetailsBruce.setReligion("Kristen");
            userDetailsBruce.setMaritalStatus("Sudah Kawin");
            userDetailsBruce.setNationality("WNA");

            authenticationService.register(userBruce, userDetailsBruce);
        }
    }
}
