package com.project.laporRTeApps;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LaporRTeAppsApplication {

	public static void main(String[] args) {
		SpringApplication.run(LaporRTeAppsApplication.class, args);
	}

}
