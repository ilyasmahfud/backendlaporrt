package com.project.laporRTeApps.module.pengumuman.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class CreateAnnouncementDto {
    private String title;
    private String mainAnnouncement;
}
