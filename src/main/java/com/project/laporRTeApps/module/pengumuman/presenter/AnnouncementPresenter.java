package com.project.laporRTeApps.module.pengumuman.presenter;

import com.project.laporRTeApps.module.pengumuman.model.CreateAnnouncementDto;
import com.project.laporRTeApps.module.pengumuman.model.GetAllAnnouncementDto;
import com.project.laporRTeApps.module.pengumuman.service.AnnouncementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("${url.map.api}")
public class AnnouncementPresenter {
    @Autowired
    private AnnouncementService announcementService;

    @GetMapping("/v1/announcements")
    public List<GetAllAnnouncementDto> allAnnouncementDtos(){return announcementService.getAllAnnouncementDtos();}

    @GetMapping("/admin/v1/announcements")
    public List<GetAllAnnouncementDto> announcementDtos(){return announcementService.getAllAnnouncementDtos();}

    @PostMapping("/admin/v1/announcement")
    public ResponseEntity<Object> createNewAnnouncement(@Valid @RequestBody CreateAnnouncementDto createAnnouncementDto){
        return announcementService.createNewAnnouncement(createAnnouncementDto);
    }
}
