package com.project.laporRTeApps.module.pengumuman.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class GetAllAnnouncementDto {
    private UUID id;
    private String full_name;
    private String title;
    private String mainAnnouncement;
    private Instant createdAt;
}
