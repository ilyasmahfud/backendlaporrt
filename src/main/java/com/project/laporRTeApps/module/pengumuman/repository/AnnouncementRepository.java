package com.project.laporRTeApps.module.pengumuman.repository;

import com.project.laporRTeApps.module.pengumuman.entity.Announcement;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface AnnouncementRepository extends JpaRepository<Announcement, UUID> {
}
