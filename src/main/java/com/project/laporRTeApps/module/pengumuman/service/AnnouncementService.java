package com.project.laporRTeApps.module.pengumuman.service;

import com.project.laporRTeApps.module.pengumuman.model.CreateAnnouncementDto;
import com.project.laporRTeApps.module.pengumuman.model.GetAllAnnouncementDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface AnnouncementService {
    List<GetAllAnnouncementDto> getAllAnnouncementDtos();
    ResponseEntity<Object> createNewAnnouncement(CreateAnnouncementDto createAnnouncementDto);
}
