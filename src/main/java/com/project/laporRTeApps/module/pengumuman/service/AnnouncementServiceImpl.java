package com.project.laporRTeApps.module.pengumuman.service;

import com.project.laporRTeApps.exception.ResourceNotFoundException;
import com.project.laporRTeApps.module.authentication.entity.User;
import com.project.laporRTeApps.module.authentication.repository.UserRepository;
import com.project.laporRTeApps.module.authentication.service.UserService;
import com.project.laporRTeApps.module.pengumuman.entity.Announcement;
import com.project.laporRTeApps.module.pengumuman.model.CreateAnnouncementDto;
import com.project.laporRTeApps.module.pengumuman.model.GetAllAnnouncementDto;
import com.project.laporRTeApps.module.pengumuman.repository.AnnouncementRepository;
import com.project.laporRTeApps.utility.ModelMapperUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class AnnouncementServiceImpl implements AnnouncementService{
    @Autowired
    private ModelMapperUtility mapperUtility;

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AnnouncementRepository announcementRepository;

    @Override
    public List<GetAllAnnouncementDto> getAllAnnouncementDtos() {
        List<Announcement> announcements = announcementRepository.findAll();
        List<GetAllAnnouncementDto> allAnnouncementDtos = mapperListAnnouncement(announcements);
        return allAnnouncementDtos;
    }

    @Override
    public ResponseEntity<Object> createNewAnnouncement(CreateAnnouncementDto createAnnouncementDto) {
        Announcement announcement = mapperCreateAnnouncement(createAnnouncementDto);
        return new ResponseEntity<>(announcementRepository.save(announcement), HttpStatus.OK);
    }

    private List<GetAllAnnouncementDto> mapperListAnnouncement(List<Announcement> announcements){
        List<GetAllAnnouncementDto> getAllAnnouncementDtoList = new ArrayList<>();
        for (Announcement data: announcements){
            GetAllAnnouncementDto allAnnouncementDto = mapperUtility.modelMapperUtility().map(data, GetAllAnnouncementDto.class);
            allAnnouncementDto.setId(data.getId());
            allAnnouncementDto.setFull_name(data.getCreator());
            allAnnouncementDto.setTitle(data.getTitle());
            allAnnouncementDto.setMainAnnouncement(data.getMainAnnouncement());
            allAnnouncementDto.setCreatedAt(data.getCreatedDate());

            getAllAnnouncementDtoList.add(allAnnouncementDto);
        }
        return getAllAnnouncementDtoList;
    }

    private Announcement mapperCreateAnnouncement(CreateAnnouncementDto createAnnouncementDto){
        Announcement announcement = mapperUtility.modelMapperUtility().map(createAnnouncementDto, Announcement.class);
        announcement.setCreator(userRepository.findbyEmailForFullname(getSignedInUser().getEmail()));
        announcement.setTitle(createAnnouncementDto.getTitle());
        announcement.setMainAnnouncement(createAnnouncementDto.getMainAnnouncement());

        System.out.println(userRepository.findbyEmailForFullname(getSignedInUser().getEmail()));
        return announcementRepository.save(announcement);
    }

    private User getSignedInUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String email = authentication.getPrincipal().toString();
        return userRepository.findByEmail(email)
                .orElseThrow(() -> new ResourceNotFoundException("User not found"));
    }
}
