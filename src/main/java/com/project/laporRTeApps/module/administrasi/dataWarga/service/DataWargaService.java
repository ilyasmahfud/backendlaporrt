package com.project.laporRTeApps.module.administrasi.dataWarga.service;

import com.project.laporRTeApps.module.administrasi.dataWarga.model.DataWargaDto;

import java.util.List;

public interface DataWargaService {
    List<DataWargaDto> getAllWarga();
}
