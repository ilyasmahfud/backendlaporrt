package com.project.laporRTeApps.module.administrasi.mintaSurat.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class GetAllLetterDto {
    private Long id;
    private String subjectLetter;
    private String needs;
    private Instant createdDate;
    private String requester;
}
