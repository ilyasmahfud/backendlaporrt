package com.project.laporRTeApps.module.administrasi.mintaSurat.repository;

import com.project.laporRTeApps.module.administrasi.mintaSurat.entity.Letter;
import com.project.laporRTeApps.module.administrasi.mintaSurat.models.GetUserRecordRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface LetterRepository extends JpaRepository<Letter, Long> {
    @Query(name = "findUserRecordRequest", nativeQuery = true)
    List<GetUserRecordRequest> findUserRequestedLetter(@Param("userId") UUID userId);
}
