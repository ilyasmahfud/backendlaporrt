package com.project.laporRTeApps.module.administrasi.mintaSurat.entity;

import com.project.laporRTeApps.module.administrasi.mintaSurat.models.GetUserRecordRequest;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.Instant;
import java.util.UUID;

@Entity
@Table(name = "letters")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@NamedNativeQuery(
        name = "findUserRecordRequest",
        query = "SELECT id, requester, created_date AS createdDate, subject_letter AS subjectLetter, needs, user_id AS userId \n" +
                "FROM letters \n" +
                "WHERE CAST(user_id AS varchar) LIKE CONCAT ('%',:userId,'%')",
        resultSetMapping = "findUserRecord"
)
@SqlResultSetMapping(
        name = "findUserRecord",
        classes = @ConstructorResult(
                targetClass = GetUserRecordRequest.class,
                columns = {
                        @ColumnResult(name = "id", type = Long.class),
                        @ColumnResult(name = "requester", type = String.class),
                        @ColumnResult(name = "subjectLetter", type = String.class),
                        @ColumnResult(name = "needs", type = String.class),
                        @ColumnResult(name = "createdDate", type = Instant.class),
                        @ColumnResult(name = "userId", type = UUID.class)
                }
        )
)
public class Letter {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String subjectLetter;
    private String needs;

    private String requester;
    @CreationTimestamp
    private Instant createdDate;

    private UUID userId;
}
