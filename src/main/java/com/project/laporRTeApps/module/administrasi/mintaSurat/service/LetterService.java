package com.project.laporRTeApps.module.administrasi.mintaSurat.service;

import com.project.laporRTeApps.module.administrasi.mintaSurat.models.CreateLetterRequestDto;
import com.project.laporRTeApps.module.administrasi.mintaSurat.models.GetAllLetterDto;
import com.project.laporRTeApps.module.administrasi.mintaSurat.models.GetUserRecordRequest;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface LetterService {
    List<GetAllLetterDto> getAllRequestedLetter();
    List<GetUserRecordRequest> getAllUserRecordRequestLetter();
    ResponseEntity<Object> createNewRequestLetter(CreateLetterRequestDto createLetterRequestDto);
}
