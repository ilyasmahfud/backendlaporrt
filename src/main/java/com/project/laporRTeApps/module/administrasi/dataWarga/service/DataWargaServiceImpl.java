package com.project.laporRTeApps.module.administrasi.dataWarga.service;

import com.project.laporRTeApps.module.administrasi.dataWarga.model.DataWargaDto;
import com.project.laporRTeApps.module.authentication.entity.UserDetails;
import com.project.laporRTeApps.module.authentication.repository.UserDetailsRepository;
import com.project.laporRTeApps.utility.ModelMapperUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DataWargaServiceImpl implements DataWargaService{
    @Autowired
    private ModelMapperUtility mapperUtility;

    @Autowired
    private UserDetailsRepository userDetailsRepository;

    @Override
    public List<DataWargaDto> getAllWarga() {
        List<UserDetails> userDetails = userDetailsRepository.findAll();
        List<DataWargaDto> dataWargaDtoList = mapperDataWarga(userDetails);
        return dataWargaDtoList;
    }

    private List<DataWargaDto> mapperDataWarga(List<UserDetails> warga){
        List<DataWargaDto> dataWargaDtoList = new ArrayList<>();
        for (UserDetails data: warga){
            DataWargaDto dataWargaDto = mapperUtility.modelMapperUtility().map(data, DataWargaDto.class);
            dataWargaDto.setName(data.getFullName());
            dataWargaDto.setAddress(data.getAddress());
            dataWargaDto.setMobileNumber(data.getMobileNumber());

            dataWargaDtoList.add(dataWargaDto);
        }
        return dataWargaDtoList;
    }
}
