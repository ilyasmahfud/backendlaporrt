package com.project.laporRTeApps.module.administrasi.dataWarga.presenter;

import com.project.laporRTeApps.module.administrasi.dataWarga.model.DataWargaDto;
import com.project.laporRTeApps.module.administrasi.dataWarga.service.DataWargaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("${url.map.api}")
public class DataWargaPresenter {
    @Autowired
    private DataWargaService dataWargaService;

    @GetMapping("/v1/warga")
    public List<DataWargaDto> getAllWarga(){return dataWargaService.getAllWarga();}
}
