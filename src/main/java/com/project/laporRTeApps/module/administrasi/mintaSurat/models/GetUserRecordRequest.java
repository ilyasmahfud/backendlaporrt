package com.project.laporRTeApps.module.administrasi.mintaSurat.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class GetUserRecordRequest {
    private Long id;
    private String requester;
    private String subjectLetter;
    private String needs;
    private Instant createdDate;
    private UUID userId;
}
