package com.project.laporRTeApps.module.administrasi.mintaSurat.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class CreateLetterRequestDto {
    private String requester;
    private String subjectLetter;
    private String needs;
}
