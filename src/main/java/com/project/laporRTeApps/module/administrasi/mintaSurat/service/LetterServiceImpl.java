package com.project.laporRTeApps.module.administrasi.mintaSurat.service;

import com.project.laporRTeApps.exception.ResourceNotFoundException;
import com.project.laporRTeApps.module.administrasi.mintaSurat.entity.Letter;
import com.project.laporRTeApps.module.administrasi.mintaSurat.models.CreateLetterRequestDto;
import com.project.laporRTeApps.module.administrasi.mintaSurat.models.GetAllLetterDto;
import com.project.laporRTeApps.module.administrasi.mintaSurat.models.GetUserRecordRequest;
import com.project.laporRTeApps.module.administrasi.mintaSurat.repository.LetterRepository;
import com.project.laporRTeApps.module.authentication.entity.User;
import com.project.laporRTeApps.module.authentication.repository.UserDetailsRepository;
import com.project.laporRTeApps.module.authentication.repository.UserRepository;
import com.project.laporRTeApps.module.authentication.service.UserService;
import com.project.laporRTeApps.utility.ModelMapperUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class LetterServiceImpl implements LetterService{
    @Autowired
    private ModelMapperUtility mapperUtility;

    @Autowired
    private LetterRepository letterRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserDetailsRepository userDetailsRepository;

    @Override
    public List<GetAllLetterDto> getAllRequestedLetter() {
        List<Letter> letters = letterRepository.findAll();
        List<GetAllLetterDto> getAllLetterDtoList = mapperRequestedLetter(letters);
        return getAllLetterDtoList;
    }

    @Override
    public List<GetUserRecordRequest> getAllUserRecordRequestLetter() {
        return letterRepository.findUserRequestedLetter(getSignedInUser().getUserId());
    }

    @Override
    public ResponseEntity<Object> createNewRequestLetter(CreateLetterRequestDto createLetterRequestDto) {
        Letter letter = mapperCreateNewRequestLetter(createLetterRequestDto);
        return new ResponseEntity<>(letterRepository.save(letter), HttpStatus.CREATED);
    }

    private List<GetAllLetterDto> mapperRequestedLetter(List<Letter> letters){
        List<GetAllLetterDto> getAllLetterDtos = new ArrayList<>();
        for (Letter data: letters){
            GetAllLetterDto allLetterDto = mapperUtility.modelMapperUtility().map(data, GetAllLetterDto.class);
            allLetterDto.setId(data.getId());
            allLetterDto.setSubjectLetter(data.getSubjectLetter());
            allLetterDto.setNeeds(data.getNeeds());
            allLetterDto.setRequester(data.getRequester());
            allLetterDto.setCreatedDate(data.getCreatedDate());

            getAllLetterDtos.add(allLetterDto);
        }
        return getAllLetterDtos;
    }



    private Letter mapperCreateNewRequestLetter(CreateLetterRequestDto createLetterRequestDto){
        Letter letter = mapperUtility.modelMapperUtility().map(createLetterRequestDto, Letter.class);
        letter.setSubjectLetter(createLetterRequestDto.getSubjectLetter());
        letter.setNeeds(createLetterRequestDto.getNeeds());
        letter.setRequester(userRepository.findbyEmailForFullname(getSignedInUser().getEmail()));
        letter.setUserId(getSignedInUser().getUserId());

        return letterRepository.save(letter);
    }

    private User getSignedInUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String email = authentication.getPrincipal().toString();
        return userRepository.findByEmail(email)
                .orElseThrow(() -> new ResourceNotFoundException("User not found"));
    }
}
