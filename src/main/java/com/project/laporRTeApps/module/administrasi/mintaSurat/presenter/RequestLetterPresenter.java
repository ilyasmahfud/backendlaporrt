package com.project.laporRTeApps.module.administrasi.mintaSurat.presenter;

import com.project.laporRTeApps.module.administrasi.mintaSurat.models.CreateLetterRequestDto;
import com.project.laporRTeApps.module.administrasi.mintaSurat.models.GetAllLetterDto;
import com.project.laporRTeApps.module.administrasi.mintaSurat.models.GetUserRecordRequest;
import com.project.laporRTeApps.module.administrasi.mintaSurat.service.LetterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("${url.map.api}")
public class RequestLetterPresenter {
    @Autowired
    private LetterService letterService;

    @RequestMapping(value = "/v1/requestedLetters", method = RequestMethod.GET)
    public List<GetUserRecordRequest> getUserRecordRequestList(){
        return letterService.getAllUserRecordRequestLetter();
    }

    @RequestMapping(value = "/admin/v1/requestedLetters", method = RequestMethod.GET)
    public List<GetAllLetterDto> getAllLetterDtos(){return letterService.getAllRequestedLetter();}

    @PostMapping("/v1/requestedLetter/create")
    public ResponseEntity<Object> createLetterRequest (@Valid @RequestBody CreateLetterRequestDto createLetterRequestDto){
        return letterService.createNewRequestLetter(createLetterRequestDto);
    }
}
