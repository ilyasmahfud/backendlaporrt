package com.project.laporRTeApps.module.aduan.presenter;

import com.project.laporRTeApps.module.aduan.model.CreateNewReportDto;
import com.project.laporRTeApps.module.aduan.model.DetailReportDto;
import com.project.laporRTeApps.module.aduan.model.GetAllReportDto;
import com.project.laporRTeApps.module.aduan.model.admin.AdminUpdateReportDto;
import com.project.laporRTeApps.module.aduan.model.user.*;
import com.project.laporRTeApps.module.aduan.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("${url.map.api}")
public class ReportByWargaPresenter {
    @Autowired
    private ReportService reportService;

    @RequestMapping(value = "/v1/reports", method = RequestMethod.GET)
    public List<GetAllReportDto> getAllReportsFromUser(){
        return reportService.getAllReport();
    }

    @RequestMapping(path = "/v1/report/getDetails")
    public ResponseEntity<DetailReportDto> getSingleReport(@RequestParam int id){
        return reportService.getDetailReport(Long.valueOf(id));
    }

    @GetMapping("/v1/userReports")
    public List<ReportHistoryDto> getUserReportHistory(){return reportService.getUserReportHistory();}

    @PostMapping("/v1/report")
    public ResponseEntity<Object> createNewReport(@Valid @RequestBody CreateNewReportDto createNewReportDto){
        return reportService.createNewReport(createNewReportDto);
    }

    @PutMapping("/v1/report/updateReport")
    public ResponseEntity<Object> updateReport(@RequestParam int id,
                                               @RequestBody UpdateReportDto updateReportDto){
        return reportService.updateReport(Long.valueOf(id), updateReportDto);
    }

    @PutMapping("/admin/v1/report/updateReport")
    public ResponseEntity<Object> updateReport(@RequestParam int id,
                                               @RequestBody AdminUpdateReportDto adminUpdateReportDto){
        return reportService.adminUpdateReport(Long.valueOf(id), adminUpdateReportDto);
    }

    @DeleteMapping("/v1/report/deleteReport")
    public ResponseEntity<Object> deleteReport(@RequestParam int id){
        return reportService.deteteReport(Long.valueOf(id));
    }
}
