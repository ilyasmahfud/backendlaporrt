package com.project.laporRTeApps.module.aduan.entity;

import com.project.laporRTeApps.module.aduan.model.user.ReportHistoryDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.Instant;
import java.util.UUID;

@Entity
@Table(name = "reports")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@NamedNativeQuery(
        name = "findReportHistoryUserByEmail",
        query =
                "SELECT reports.id AS id, reports.user_id AS userId, main_report AS mainReport, reports.created_date AS createdAt, reports.status AS status, reports.title AS title\n" +
                        "\t FROM (SELECT full_name, user_details.user_id, users.user_id, users.email AS email\n" +
                        "\t FROM user_details \n" +
                        "\t INNER JOIN users\n" +
                        "\t ON users.user_id = user_details.user_id ) AS usera\n" +
                        "\t INNER JOIN reports\n" +
                        "\t ON reports.email_creator = usera.email" +
                        "\t WHERE reports.email_creator LIKE CONCAT ('%',:email,'%')",
        resultSetMapping = "findReportHistory"
)
@SqlResultSetMapping(
        name = "findReportHistory",
        classes = @ConstructorResult(
                targetClass = ReportHistoryDto.class,
                columns = {
                        @ColumnResult(name = "id", type = String.class),
                        @ColumnResult(name = "title", type = String.class),
                        @ColumnResult(name = "createdAt", type = Instant.class),
                        @ColumnResult(name = "mainReport", type = String.class),
                        @ColumnResult(name = "status", type = Boolean.class),
                        @ColumnResult(name = "userId", type = String.class)
                }
        )
)
public class Report {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String reporter;
    private String title;
    private String mainReport;
    private String images;

    private String emailCreator;

    private String notes;
    private Boolean status;
    private String pengurus;

    @CreationTimestamp
    private Instant createdDate;
    @UpdateTimestamp
    private Instant modifiedDate;

    private UUID userId;
}
