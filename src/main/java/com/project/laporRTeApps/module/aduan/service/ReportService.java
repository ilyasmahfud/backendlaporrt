package com.project.laporRTeApps.module.aduan.service;

import com.project.laporRTeApps.module.aduan.model.CreateNewReportDto;
import com.project.laporRTeApps.module.aduan.model.DetailReportDto;
import com.project.laporRTeApps.module.aduan.model.GetAllReportDto;
import com.project.laporRTeApps.module.aduan.model.admin.AdminUpdateReportDto;
import com.project.laporRTeApps.module.aduan.model.user.*;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ReportService {
    List<GetAllReportDto> getAllReport();
    List<ReportHistoryDto> getUserReportHistory();
    ResponseEntity<DetailReportDto> getDetailReport(Long id);
    ResponseEntity<Object> createNewReport(CreateNewReportDto createNewReportDto);
    ResponseEntity<Object> updateReport(Long id, UpdateReportDto updateReportDto);
    ResponseEntity<Object> adminUpdateReport(Long id, AdminUpdateReportDto adminUpdateReportDto);
    ResponseEntity<Object> deteteReport(Long id);
}