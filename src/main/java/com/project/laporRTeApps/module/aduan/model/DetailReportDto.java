package com.project.laporRTeApps.module.aduan.model;

import com.project.laporRTeApps.module.authentication.entity.User;
import lombok.*;

import java.time.Instant;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class DetailReportDto {
    private Long id;
    private String reporter;
    private String title;
    private String mainReport;
    private String images;
    private Boolean status;

    private String notes;
    private String pengurus;

    private Instant createdDate;
    private Instant modifiedDate;

    private User user;
}
