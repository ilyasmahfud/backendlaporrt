package com.project.laporRTeApps.module.aduan.model.user;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class UpdateReportDto {
    private String mainReport;
}
