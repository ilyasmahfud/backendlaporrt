package com.project.laporRTeApps.module.aduan.model.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ReportHistoryDto {
    private String id;
    private String title;
    private Instant createdAt;
    private String mainReport;
    private Boolean status;
    private String userId;
}
