package com.project.laporRTeApps.module.aduan.model;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class CreateNewReportDto {
    private String reporter;
    private String title;
    private String mainReport;
    private String images;
    private String emailCreator;
}
