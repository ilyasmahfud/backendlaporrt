package com.project.laporRTeApps.module.aduan.repository;

import com.project.laporRTeApps.module.aduan.entity.Report;
import com.project.laporRTeApps.module.aduan.model.user.ReportHistoryDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReportRepository extends JpaRepository<Report, Long> {
//    @Query( value = "SELECT report_id FROM reports WHERE" +
//            "CAST(report_id AS VARCHAR) LIKE %:reportId%",nativeQuery = true)
//    Optional<Report> findDetails(@Param("reportId") String reportId);
//    Optional<Report> findByReportId(Long id);
    @Query(name = "findReportHistoryUserByEmail", nativeQuery = true)
    List<ReportHistoryDto> findUserReportHistory(@Param("email")String email);
}
