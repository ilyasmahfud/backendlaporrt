package com.project.laporRTeApps.module.aduan.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class GetAllReportDto {
    private Long id;
    private String reporter;
    private String title;
    private String mainReport;
    private String images;
    private Boolean status;
}
