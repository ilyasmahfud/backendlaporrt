package com.project.laporRTeApps.module.aduan.service;

import com.project.laporRTeApps.exception.ResourceNotFoundException;
import com.project.laporRTeApps.module.aduan.entity.Report;
import com.project.laporRTeApps.module.aduan.model.CreateNewReportDto;
import com.project.laporRTeApps.module.aduan.model.DetailReportDto;
import com.project.laporRTeApps.module.aduan.model.GetAllReportDto;
import com.project.laporRTeApps.module.aduan.model.admin.AdminUpdateReportDto;
import com.project.laporRTeApps.module.aduan.model.user.*;
import com.project.laporRTeApps.module.aduan.repository.ReportRepository;
import com.project.laporRTeApps.module.authentication.entity.User;
import com.project.laporRTeApps.module.authentication.repository.UserDetailsRepository;
import com.project.laporRTeApps.module.authentication.repository.UserRepository;
import com.project.laporRTeApps.module.authentication.service.UserService;
import com.project.laporRTeApps.utility.ModelMapperUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ReportServiceImpl implements ReportService{
    @Autowired
    private ModelMapperUtility mapperUtility;

    @Autowired
    private ReportRepository reportRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserDetailsRepository userDetailsRepository;

    @Override
    public List<GetAllReportDto> getAllReport() {
        List<Report> reports = reportRepository.findAll();
        List<GetAllReportDto> allReportDtoList = mapperClassDto(reports);
        return allReportDtoList;
    }

    @Override
    public List<ReportHistoryDto> getUserReportHistory() {
        return reportRepository.findUserReportHistory(getSignedInUser().getEmail());
    }

    @Override
    public ResponseEntity<DetailReportDto> getDetailReport(@PathVariable Long id) {
        Optional<Report> reportDetail = Optional.ofNullable(reportRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("report not found")));
        DetailReportDto detailReportDto = mapperClassDetailToDto(reportDetail);
        return new ResponseEntity<>(detailReportDto, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> createNewReport(CreateNewReportDto createNewReportDto) {
        Report report = mapperCreateReportToEntity(createNewReportDto);
        return new ResponseEntity<>(reportRepository.save(report), HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Object> updateReport(Long id,
                                               UpdateReportDto updateReportDto) {
        Report report = mapperUpdateReport(id, updateReportDto);
        return new ResponseEntity<>(report, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> adminUpdateReport(Long id,
                                                    AdminUpdateReportDto adminUpdateReportDto) {
        Report report = mapperAdminUpdateReport(id, adminUpdateReportDto);
        return new ResponseEntity<>(report, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> deteteReport(Long id) {
        Report report_detail = reportRepository.findById(id)
                .orElseThrow(()->new ResourceNotFoundException("Report not found"));
        reportRepository.delete(report_detail);
        return new ResponseEntity<>(report_detail, HttpStatus.OK);
    }

    private List<GetAllReportDto> mapperClassDto(List<Report> reports){
        List<GetAllReportDto> getAllReportDtoList = new ArrayList<>();
        for (Report data: reports){
            GetAllReportDto allReportDto = mapperUtility.modelMapperUtility().map(data, GetAllReportDto.class);
            allReportDto.setId(data.getId());
            allReportDto.setReporter(data.getReporter());
            allReportDto.setTitle(data.getTitle());
            allReportDto.setMainReport(data.getMainReport());
            allReportDto.setImages(data.getImages());
            allReportDto.setStatus(data.getStatus());

            getAllReportDtoList.add(allReportDto);
        }
        return getAllReportDtoList;
    }

    private DetailReportDto mapperClassDetailToDto(Optional<Report> reports){
        DetailReportDto detailReportDto = mapperUtility.modelMapperUtility().map(reports, DetailReportDto.class);
        reports.ifPresent(value->{
            detailReportDto.setId(value.getId());
            detailReportDto.setReporter(value.getReporter());
            detailReportDto.setTitle(value.getTitle());
            detailReportDto.setMainReport(value.getMainReport());
            detailReportDto.setImages(value.getImages());
            detailReportDto.setStatus(value.getStatus());
            detailReportDto.setNotes(value.getNotes());
            detailReportDto.setCreatedDate(value.getCreatedDate());
            detailReportDto.setModifiedDate(value.getModifiedDate());
            detailReportDto.setPengurus(value.getPengurus());
        });
        return detailReportDto;
    }

    private Report mapperCreateReportToEntity(CreateNewReportDto createNewReportDto){
        Report report = mapperUtility.modelMapperUtility().map(createNewReportDto, Report.class);
        report.setReporter(userRepository.findbyEmailForFullname(getSignedInUser().getEmail()));
        report.setEmailCreator(getSignedInUser().getEmail());
        report.setTitle(createNewReportDto.getTitle());
        report.setMainReport(createNewReportDto.getMainReport());
        report.setImages(createNewReportDto.getImages());
        report.setStatus(false);
        report.setNotes("none");
        report.setPengurus("none");
        report.setUserId(getSignedInUser().getUserId());

        return reportRepository.save(report);
    }

    private Report mapperUpdateReport(Long id, UpdateReportDto updateReportDto){
        Report reportById = reportRepository.findById(id)
                .orElseThrow(()->new ResourceNotFoundException("report not found"));
        Report report = mapperUtility.modelMapperUtility().map(updateReportDto, Report.class);
        report.setMainReport(updateReportDto.getMainReport());
        reportById.setMainReport(report.getMainReport());

        return reportRepository.save(reportById);
    }

    private Report mapperAdminUpdateReport(Long id, AdminUpdateReportDto adminUpdateReportDto){
        Report reportbyId = reportRepository.findById(id)
                .orElseThrow(()->new ResourceNotFoundException("report not found"));
        Report reportMapper = mapperUtility.modelMapperUtility().map(adminUpdateReportDto, Report.class);
        reportMapper.setStatus(adminUpdateReportDto.getStatus());
        reportMapper.setNotes(adminUpdateReportDto.getNotes());
        reportMapper.setPengurus(userRepository.findbyEmailForFullname(getSignedInUser().getEmail()));

        System.out.println(userRepository.findbyEmailForFullname(getSignedInUser().getEmail()));

        reportbyId.setStatus(reportMapper.getStatus());
        reportbyId.setNotes(reportMapper.getNotes());
        reportbyId.setPengurus(reportMapper.getPengurus());

        return reportRepository.save(reportbyId);
    }

    private User getSignedInUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String email = authentication.getPrincipal().toString();
        return userRepository.findByEmail(email)
                .orElseThrow(() -> new ResourceNotFoundException("User not found"));
    }
}