package com.project.laporRTeApps.module.aduan.model.admin;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class AdminUpdateReportDto {
    private Boolean status;
    private String notes;
    private String pengurus;
}
