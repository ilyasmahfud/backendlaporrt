package com.project.laporRTeApps.module.authentication.service;

import com.project.laporRTeApps.exception.AuthenticateFailedException;
import com.project.laporRTeApps.exception.ForbiddenException;
import com.project.laporRTeApps.exception.RegisterFailedException;
import com.project.laporRTeApps.exception.ResourceNotFoundException;
import com.project.laporRTeApps.module.authentication.entity.Role;
import com.project.laporRTeApps.module.authentication.entity.RoleEnum;
import com.project.laporRTeApps.module.authentication.entity.User;
import com.project.laporRTeApps.module.authentication.entity.UserDetails;
import com.project.laporRTeApps.module.authentication.model.LoginAdminDto;
import com.project.laporRTeApps.module.authentication.model.LoginDto;
import com.project.laporRTeApps.module.authentication.repository.RoleRepository;
import com.project.laporRTeApps.module.authentication.repository.UserDetailsRepository;
import com.project.laporRTeApps.module.authentication.repository.UserRepository;
import com.project.laporRTeApps.utility.FakerUtility;
import com.project.laporRTeApps.utility.ModelMapperUtility;
import com.project.laporRTeApps.utility.RestTemplateUtility;
import com.project.laporRTeApps.utility.UserUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {
    @Value("${resource.server.url}")
    private String BASE_URL;

    @Autowired
    private FakerUtility fakerUtility;
    @Autowired
    private ModelMapperUtility modelMapperUtility;
    @Autowired
    private RestTemplateUtility restTemplateUtility;
    @Autowired
    private UserUtility userUtility;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserDetailsRepository userDetailsRepository;

    @Override
    @Transactional
    public void register(User userRequestBody, UserDetails userDetailsRequestBody) {
        if (userDetailsRepository.findByMobileNumber(userDetailsRequestBody.getMobileNumber()).isPresent() ||
                userRepository.findByEmail(userRequestBody.getEmail()).isPresent()) {
            throw new RegisterFailedException("Email address or phone number already registered");
        } else if (userDetailsRepository.findByNik(userDetailsRequestBody.getNik()).isPresent()){
            throw new RegisterFailedException("NIK already registered");
        }

        Role userRole = roleRepository.findByRoleName(RoleEnum.USER)
                .orElseThrow(() -> new ResourceNotFoundException("Role not found"));

        User newUser = userRepository.save(
                new User(
                        userRequestBody.getEmail(),
                        passwordEncoder.encode(userRequestBody.getPassword()),
                        userRole));

        UserDetails newUserDetails = modelMapperUtility.modelMapperUtility()
                .map(userDetailsRequestBody, UserDetails.class);
        newUserDetails.setUser(newUser);
        newUserDetails.setProfilePicture("https://cdn.kibrispdr.org/data/icon-profile-png-0.jpg");
        userDetailsRepository.save(newUserDetails);
    }

    @Override
    public LoginDto login(String email, String password) {
        try {
            Authentication authentication = authenticationManager
                    .authenticate(new UsernamePasswordAuthenticationToken(email, password));
            SecurityContextHolder.getContext().setAuthentication(authentication);

            User user = userRepository.findByEmail(authentication.getName())
                    .orElseThrow(() -> new ResourceNotFoundException("User not found"));
            if (user.getRole().getRoleName().name().equals(RoleEnum.USER.name())) {
                UserDetails userDetails = userDetailsRepository.findByUser(user)
                        .orElseThrow(() -> new ResourceNotFoundException("User details not found"));

                LoginDto loginDto = new LoginDto();
                loginDto.setFullName(userDetails.getFullName());
                loginDto.setEmail(user.getEmail());
                loginDto.setRole(user.getRole().getRoleName().name());
                return loginDto;
            } else {
                throw new ForbiddenException("Forbidden");
            }
        } catch (BadCredentialsException exception) {
            throw new AuthenticateFailedException("Incorrect email or password");
        }
    }

    @Override
    @Transactional
    public void registerAdmin(User userRequestBody, UserDetails userDetailsRequestBody) {
        if (userDetailsRepository.findByMobileNumber(userDetailsRequestBody.getMobileNumber()).isPresent() ||
                userRepository.findByEmail(userRequestBody.getEmail()).isPresent()) {
            throw new RegisterFailedException("Email address or phone number already registered");
        } else if (userDetailsRepository.findByNik(userDetailsRequestBody.getNik()).isPresent()){
            throw new RegisterFailedException("NIK already registered");
        }

        Role userRole = roleRepository.findByRoleName(RoleEnum.ADMIN)
                .orElseThrow(() -> new ResourceNotFoundException("Role not found"));

        User newUser = userRepository.save(
                new User(
                        userRequestBody.getEmail(),
                        passwordEncoder.encode(userRequestBody.getPassword()),
                        userRole));

        UserDetails newUserDetails = modelMapperUtility.modelMapperUtility()
                .map(userDetailsRequestBody, UserDetails.class);
        newUserDetails.setUser(newUser);
        newUserDetails.setProfilePicture("https://cdn.kibrispdr.org/data/icon-profile-png-0.jpg");
        userDetailsRepository.save(newUserDetails);
    }

    @Override
    public LoginAdminDto loginAdmin(String email, String password) {
        try {
            Authentication authentication = authenticationManager
                    .authenticate(new UsernamePasswordAuthenticationToken(email, password));
            SecurityContextHolder.getContext().setAuthentication(authentication);

            User user = userRepository.findByEmail(authentication.getName())
                    .orElseThrow(() -> new ResourceNotFoundException("User not found"));
            if (user.getRole().getRoleName().name().equals(RoleEnum.ADMIN.name())) {
                LoginAdminDto loginAdminDto = new LoginAdminDto();
                loginAdminDto.setEmail(user.getEmail());
                loginAdminDto.setRole(user.getRole().getRoleName().name());
                return loginAdminDto;
            } else {
                throw new ForbiddenException("Forbidden");
            }
        } catch (BadCredentialsException exception) {
            throw new AuthenticateFailedException("Incorrect email or password");
        }
    }

}
