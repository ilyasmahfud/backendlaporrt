package com.project.laporRTeApps.module.authentication.repository;

import com.project.laporRTeApps.module.authentication.entity.User;
import com.project.laporRTeApps.module.authentication.entity.UserDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface UserDetailsRepository extends JpaRepository<UserDetails, UUID> {
    Optional<UserDetails> findByMobileNumber(String mobileNumber);
    Optional<UserDetails> findByNik (String nik);
    Optional<UserDetails> findByUser(User user);
}
