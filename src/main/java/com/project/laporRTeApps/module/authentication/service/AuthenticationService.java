package com.project.laporRTeApps.module.authentication.service;

import com.project.laporRTeApps.module.authentication.entity.User;
import com.project.laporRTeApps.module.authentication.entity.UserDetails;
import com.project.laporRTeApps.module.authentication.model.LoginAdminDto;
import com.project.laporRTeApps.module.authentication.model.LoginDto;

public interface AuthenticationService {
    void register(User userRequestBody, UserDetails userDetailsRequestBody);
    LoginDto login(String email, String password);
    void registerAdmin(User userRequestBody, UserDetails userDetailsRequestBody);
    LoginAdminDto loginAdmin(String email, String password);
}
