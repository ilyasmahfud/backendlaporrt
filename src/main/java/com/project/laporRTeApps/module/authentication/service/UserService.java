package com.project.laporRTeApps.module.authentication.service;

import com.project.laporRTeApps.module.authentication.model.UserDetailsDto;
import com.project.laporRTeApps.module.authentication.model.UserProfileDto;

public interface UserService {
    UserDetailsDto getUserDetails();
    UserProfileDto getUserProfile();
}
