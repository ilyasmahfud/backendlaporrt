package com.project.laporRTeApps.module.authentication.presenter.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class RegisterRequest {
    @NotBlank
    private String fullName;
    @NotBlank
    private String mobileNumber;
    @NotBlank
    private String nik;
    @NotBlank
    private String rt;
    @NotBlank
    private String placeOfBirth;
    @NotBlank
    private String dateOfBirth;
    @NotBlank
    private String gender;
    @NotBlank
    private String address;
    @NotBlank
    private String work;
    @NotBlank
    private String religion;
    @NotBlank
    private String maritalStatus;
    @NotBlank
    private String nationality;
    @NotBlank
    @Email
    private String email;
    @NotBlank
    @Size(min = 8)
    private String password;
}