package com.project.laporRTeApps.module.authentication.presenter.controller;

import com.project.laporRTeApps.model.SuccessDetailsResponse;
import com.project.laporRTeApps.model.SuccessResponse;
import com.project.laporRTeApps.module.authentication.entity.User;
import com.project.laporRTeApps.module.authentication.entity.UserDetails;
import com.project.laporRTeApps.module.authentication.entity.UserDetailsImpl;
import com.project.laporRTeApps.module.authentication.jwt.JwtTokenUtility;
import com.project.laporRTeApps.module.authentication.model.LoginAdminDto;
import com.project.laporRTeApps.module.authentication.model.LoginDto;
import com.project.laporRTeApps.module.authentication.presenter.model.LoginRequest;
import com.project.laporRTeApps.module.authentication.presenter.model.RegisterRequest;
import com.project.laporRTeApps.module.authentication.service.AuthenticationService;
import com.project.laporRTeApps.utility.ModelMapperUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("${url.map.api}")
public class AuthenticationController {
    @Autowired
    private ModelMapperUtility modelMapperUtility;
    @Autowired
    private JwtTokenUtility jwtTokenUtility;
    @Autowired
    private AuthenticationService authenticationService;

    @PostMapping(
            path = "/register",
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> register(@RequestBody RegisterRequest requestBody) {
        User newUser = modelMapperUtility.modelMapperUtility()
                .map(requestBody, User.class);
        UserDetails newUserDetails = modelMapperUtility.modelMapperUtility()
                .map(requestBody, UserDetails.class);
        authenticationService.register(newUser, newUserDetails);
        return new ResponseEntity<>(
                new SuccessResponse(true, "Warga successfully registered"),
                HttpStatus.CREATED);
    }

    @PostMapping(
            path = "/login",
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<Object> login(@RequestBody LoginRequest requestBody) {
        LoginDto loginDto = authenticationService.login(requestBody.getEmail(), requestBody.getPassword());
        UserDetailsImpl userDetails = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String token = jwtTokenUtility.generateToken(userDetails);
        loginDto.setToken(token);
        return ResponseEntity.ok()
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + token)
                .body(new SuccessDetailsResponse(true, "Login successful", loginDto));
    }

    @PostMapping(
            path = "/register/admin",
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> registerAdmin(@RequestBody RegisterRequest requestBody) {
        User newUser = modelMapperUtility.modelMapperUtility()
                .map(requestBody, User.class);
        UserDetails newUserDetails = modelMapperUtility.modelMapperUtility()
                .map(requestBody, UserDetails.class);
        authenticationService.registerAdmin(newUser, newUserDetails);
        return new ResponseEntity<>(
                new SuccessResponse(true, "Admin successfully registered"),
                HttpStatus.CREATED);
    }

    @PostMapping(
            path = "/admin/login",
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> loginAdmin(@RequestBody LoginRequest requestBody) {
        LoginAdminDto loginAdminDto = authenticationService.loginAdmin(requestBody.getEmail(), requestBody.getPassword());
        UserDetailsImpl userDetails = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String token = jwtTokenUtility.generateToken(userDetails);
        loginAdminDto.setToken(token);
        return ResponseEntity.ok()
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + token)
                .body(new SuccessDetailsResponse(true, "Login successful", loginAdminDto));
    }
}
