package com.project.laporRTeApps.module.authentication.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class UserDetailsDto {
    private String fullName;
    private String email;
    private String mobileNumber;
    private String password;
    private String nik;
    private String profilePicture;
    private String placeOfBirth;
    private String dateOfBirth;
    private String gender;
    private String address;
    private String rt;
    private String work;
    private String religion;
    private String maritalStatus;
    private String nationality;
}
