package com.project.laporRTeApps.exception;

public class MobilePhoneNumberNotValidException extends RuntimeException {
    public MobilePhoneNumberNotValidException(String message) {
        super(message);
    }
}
