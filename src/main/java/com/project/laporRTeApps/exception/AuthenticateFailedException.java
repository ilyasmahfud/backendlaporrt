package com.project.laporRTeApps.exception;

public class AuthenticateFailedException extends RuntimeException {
    public AuthenticateFailedException(String message) {
        super(message);
    }
}
